load IterativePow.vm,
output-file IterativePow.out,
compare-to IterativePow.cmp,
output-list RAM[5]%D1.6.1,

set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 0, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 1, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 2, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 3, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 4, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 3, // temp0=A
set RAM[6] 2, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 3, // temp0=A
set RAM[6] 6, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 6, // temp0=A
set RAM[6] 3, // temp1=B

repeat 1000 {
  vmstep;
}
output;


load IterativePow.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 0, // temp0=A
set RAM[6] 3, // temp1=B

repeat 1000 {
  vmstep;
}
output;
