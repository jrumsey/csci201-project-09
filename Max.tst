load Max.vm,
output-file Max.out,
compare-to Max.cmp,
output-list RAM[5]%D1.6.1,

set sp 256,
set argument 256,
set local 256,
set RAM[5] 4, // temp0=A
set RAM[6] 3, // temp1=B

repeat 50 {
  vmstep;
}
output;

load Max.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] -1, // temp0=A
set RAM[6] 3, // temp1=B

repeat 50 {
  vmstep;
}
output;

load Max.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 0, // temp0=A
set RAM[6] 0, // temp1=B

repeat 50 {
  vmstep;
}
output;

load Max.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 3, // temp0=A
set RAM[6] -1, // temp1=B

repeat 50 {
  vmstep;
}
output;

