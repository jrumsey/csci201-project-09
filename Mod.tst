load Mod.vm,
output-file Mod.out,
compare-to Mod.cmp,
output-list RAM[5]%D1.6.1,

set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 1, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Mod.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 2, // temp0=A
set RAM[6] 2, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Mod.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 7, // temp0=A
set RAM[6] 2, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Mod.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 12, // temp0=A
set RAM[6] 5, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Mod.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 5, // temp0=A
set RAM[6] 12, // temp1=B

repeat 200 {
  vmstep;
}
output;

