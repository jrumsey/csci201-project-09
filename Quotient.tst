load Quotient.vm,
output-file Quotient.out,
compare-to Quotient.cmp,
output-list RAM[5]%D1.6.1,

set sp 256,
set argument 256,
set local 256,
set RAM[5] 4, // temp0=A
set RAM[6] 3, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 3, // temp0=A
set RAM[6] 4, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 12, // temp0=A
set RAM[6] 4, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 4, // temp0=A
set RAM[6] 12, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 0, // temp0=A
set RAM[6] 0, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 100, // temp0=A
set RAM[6] 100, // temp1=B

repeat 200 {
  vmstep;
}
output;


load Quotient.vm,
set sp 256,
set argument 256,
set local 256,
set RAM[5] 100, // temp0=A
set RAM[6] 10, // temp1=B

repeat 200 {
  vmstep;
}
output;
